# README #

Weather App Notes

Setup :

➢	Ensure Android Studio &  Gradle are installed correctly.


General Notes:

➢	Application use MVP architecture, presenter is responsible for   fetching   data and passing data to be shown. Presenter is bind to view through an interface.
 
➢	Retrofit is used for asynchronous data fetch and GSON for data parsing.

➢	Application uses Interfaces for abstraction as necessary. 

➢	Picasso is used to load images asynchronously. 

➢	Application uses Metric units of Open Weather API.

➢	Fragments have been used for display data. Application is tested on Samsung S6(OS 6.0)


Techincal Debts:

➢	Use of Dependency Injection using Dagger 

➢	Add unit test cases using Mockito.