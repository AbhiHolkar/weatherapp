package com.weatherapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.weatherapp.fragment.WeatherDetailFragment;
import com.weatherapp.fragment.WeatherFragment;
import com.weatherapp.model.Data;

public class MainActivity extends AppCompatActivity implements WeatherFragment.OnFragmentInteractionListener {

    //private Button button;
    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = (FrameLayout) findViewById(R.id.container);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.container, new WeatherFragment()).commit();
    }

    @Override
    public void onFragmentInteraction(Data item) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, WeatherDetailFragment.newInstance(item)).commit();
    }

    /*public void loadWeatherData() {

        APIService apiService = APIClient.getAPIService();
        apiService.loadWeatherData().enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                Log.d("MainActivity","Success");
                WeatherData weatherData = response.body();

            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                Log.d("MainActivity","Exception" + t.getMessage());
            }
        });
    }*/
}
