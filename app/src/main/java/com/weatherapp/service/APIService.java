package com.weatherapp.service;

import com.weatherapp.model.Weather;
import com.weatherapp.model.WeatherData;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by abhiholkar on 03/06/2017.
 */

public interface APIService {

    //9cc95be1200f26193486b1990195d504
    @GET("forecast?id=2643743&appid=9cc95be1200f26193486b1990195d504&units=metric")
    Call<WeatherData> loadWeatherData();

}
