package com.weatherapp.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abhiholkar on 03/06/2017.
 */

public class APIClient {

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";//"http://api.openweathermap.org";

    public static APIService getAPIService(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(APIService.class);

    }

}
