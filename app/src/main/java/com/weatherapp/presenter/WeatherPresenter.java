package com.weatherapp.presenter;

/**
 * Created by abhiholkar on 03/06/2017.
 */

public interface WeatherPresenter {

    void loadWeatherData();


}
