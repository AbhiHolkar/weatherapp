package com.weatherapp.presenter;

import android.content.Context;

import com.weatherapp.model.WeatherData;
import com.weatherapp.service.APIClient;
import com.weatherapp.service.APIService;
import com.weatherapp.view.WeatherView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by abhiholkar on 03/06/2017.
 */

public class WeatherPresenterImpl implements WeatherPresenter {

    Context context;
    WeatherView weatherView;

    public WeatherPresenterImpl(Context context, WeatherView weatherView) {
        this.context = context;
        this.weatherView = weatherView;
        loadWeatherData();
    }

    @Override
    public void loadWeatherData() {
        weatherView.showLoading();
        APIService apiService = APIClient.getAPIService();
        apiService.loadWeatherData().enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                weatherView.hideLoading();
                WeatherData weatherData = response.body();
                if (weatherData != null && weatherData.getDataList() != null && !weatherData.getDataList().isEmpty()) {
                    weatherView.showData(weatherData);
                } else {
                    weatherView.showError();
                }
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                weatherView.hideLoading();
                weatherView.showError();
            }
        });
    }
}
