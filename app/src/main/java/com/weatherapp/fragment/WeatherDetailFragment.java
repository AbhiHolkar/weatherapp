package com.weatherapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weatherapp.R;
import com.weatherapp.model.Data;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WeatherDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeatherDetailFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private Data data;
    private TextView temp, tempMin, tempMax, humidity;
    private TextView pressure, seaLevel, gndLevel;
    private TextView windspeed, windDegree;

    public WeatherDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param Data Parameter 1.
     * @return A new instance of fragment WeatherDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WeatherDetailFragment newInstance(Data data) {
        WeatherDetailFragment fragment = new WeatherDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather_detail, container, false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        View view = getView();
        temp = (TextView) view.findViewById(R.id.temp);
        tempMin = (TextView) view.findViewById(R.id.tempmin);
        tempMax = (TextView) view.findViewById(R.id.tempmax);
        pressure = (TextView) view.findViewById(R.id.pressure);
        seaLevel = (TextView) view.findViewById(R.id.sealevel);
        gndLevel = (TextView) view.findViewById(R.id.gndlevel);
        windspeed = (TextView) view.findViewById(R.id.windspeed);
        windDegree = (TextView) view.findViewById(R.id.winddegree);
        temp.setText(getString(R.string.temp_text) + String.valueOf(data.getMain().getTemp()));
        tempMin.setText(getString(R.string.temp_min_text) + String.valueOf(data.getMain().getTempMin()));
        tempMax.setText(getString(R.string.temp_max_text) + String.valueOf(data.getMain().getTempMax()));
        pressure.setText(getString(R.string.pressure_text) + String.valueOf(data.getMain().getPressure()));
        seaLevel.setText(getString(R.string.sealevel_text) + String.valueOf(data.getMain().getSeaLevel()));
        gndLevel.setText(getString(R.string.gndlevel_text) + String.valueOf(data.getMain().getGrndLevel()));
        windspeed.setText(getString(R.string.wnd_speed_text) + String.valueOf(data.getWind().getSpeed()));
        windDegree.setText(getString(R.string.wnd_degree_text) + String.valueOf(data.getWind().getDeg()));
    }
}
