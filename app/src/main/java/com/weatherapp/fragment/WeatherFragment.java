package com.weatherapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.weatherapp.R;
import com.weatherapp.adapter.WeatherAdapter;
import com.weatherapp.model.Data;
import com.weatherapp.model.WeatherData;
import com.weatherapp.presenter.WeatherPresenterImpl;
import com.weatherapp.view.OnItemClickListener;
import com.weatherapp.view.WeatherView;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WeatherFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class WeatherFragment extends Fragment implements WeatherView,OnItemClickListener<Data> {

    WeatherPresenterImpl weatherPresenter;
    LinearLayout progressLayout,weatherInfo;
    RecyclerView recyclerView;
    TextView errorText,cityInfo;

    private OnFragmentInteractionListener mListener;


    public WeatherFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_weather, container, false);
        progressLayout = (LinearLayout) view.findViewById(R.id.progresslayout);
        weatherInfo = (LinearLayout)view.findViewById(R.id.weatherinfocontainer);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        cityInfo = (TextView)view.findViewById(R.id.cityinfo);
        errorText = (TextView)view.findViewById(R.id.error_text);
        return  view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        weatherPresenter = new WeatherPresenterImpl(getActivity(),this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showLoading() {
        progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressLayout.setVisibility(View.GONE);
    }

    @Override
    public void showData(WeatherData weatherData) {

        weatherInfo.setVisibility(View.VISIBLE);
        cityInfo.setText(getString(R.string.city_text) + weatherData.getCity().getName() + ", " + getString(R.string.country_text) + weatherData.getCity().getCountry()) ;
        //recyclerView.setVisibility(View.VISIBLE);
        List<Data> dataList =  weatherData.getDataList();
        WeatherAdapter adapter = new WeatherAdapter(getActivity(),dataList,this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showError() {
        errorText.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClicked(Data item) {
        //Toast.makeText(getActivity(),"item clicked",Toast.LENGTH_SHORT).show();
        mListener.onFragmentInteraction(item);

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        // TODO: Update argument type and name
        void onFragmentInteraction(Data data);
    }
}
