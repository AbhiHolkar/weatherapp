package com.weatherapp.view;

/**
 * Created by abhiholkar on 03/06/2017.
 */

public interface OnItemClickListener<T> {
    void onItemClicked(T item);

}
