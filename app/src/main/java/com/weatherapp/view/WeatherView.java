package com.weatherapp.view;

import com.weatherapp.model.WeatherData;

/**
 * Created by abhiholkar on 03/06/2017.
 */

public interface WeatherView {

    void showLoading();

    void hideLoading();

    void showData(WeatherData weatherData);

    void showError();

}
