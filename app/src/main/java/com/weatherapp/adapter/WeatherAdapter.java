package com.weatherapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.weatherapp.R;
import com.weatherapp.model.Data;
import com.weatherapp.model.Weather;
import com.weatherapp.model.WeatherData;
import com.weatherapp.view.OnItemClickListener;

import java.util.List;

/**
 * Created by abhiholkar on 03/06/2017.
 */

public class WeatherAdapter extends RecyclerView.Adapter {

    Context context;
    List<Data> dataList;
    OnItemClickListener itemClickListener;

    public WeatherAdapter(Context context, List<Data> dataList, OnItemClickListener itemClickListener) {
        this.context = context;
        this.dataList = dataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_layout,parent,false);
        return new WeatherViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Data weatherData = dataList.get(position);
        Weather weather = weatherData.getWeather().get(0);

        if(weather!=null){
            WeatherViewHolder vh = (WeatherViewHolder)holder;
            vh.dateTimeText.setText(weatherData.getDtTxt());
            vh.mainText.setText(weather.getMain());
            vh.descriptionText.setText(weather.getDescription());
            String iconUrl = "http://openweathermap.org/img/w/" + weather.getIcon() +".png";
            Picasso.with(context).load(iconUrl).resize(50,50).into(vh.getWeatherIcon());
            vh.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClicked(weatherData);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
