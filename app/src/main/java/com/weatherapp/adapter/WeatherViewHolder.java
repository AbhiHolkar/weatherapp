package com.weatherapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.weatherapp.R;

/**
 * Created by abhiholkar on 03/06/2017.
 */

public class WeatherViewHolder extends RecyclerView.ViewHolder {

    View itemView;
    TextView mainText;
    TextView dateTimeText;
    TextView descriptionText;
    ImageView weatherIcon;

    public WeatherViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        dateTimeText = (TextView) itemView.findViewById(R.id.datetimetext);
        mainText = (TextView) itemView.findViewById(R.id.maintext);
        descriptionText = (TextView) itemView.findViewById(R.id.descriptiontext);
        weatherIcon = (ImageView) itemView.findViewById(R.id.weathericon);
    }

    public View getItemView() {
        return itemView;
    }

    public TextView getDateTimeText() {
        return dateTimeText;
    }

    public TextView getMainText() {
        return mainText;
    }

    public TextView getDescriptionText() {
        return descriptionText;
    }

    public ImageView getWeatherIcon() {
        return weatherIcon;
    }
}
